US Towns Array
Greg Whitehead
http://wpcms.ninja

This is the total count of the towns in the array by total and by state.

Total Count: 25292
Alabama has 494 Towns.
Alaska has 349 Towns.
Arizona has 250 Towns.
Arkansas has 519 Towns.
California has 681 Towns.
Colorado has 381 Towns.
Connecticut has 120 Towns.
Delaware has 75 Towns.
District Of Columbia has 1 Towns.
Florida has 888 Towns.
Georgia has 596 Towns.
Hawaii has 131 Towns.
Idaho has 205 Towns.
Illinois has 1313 Towns.
Indiana has 601 Towns.
Iowa has 954 Towns.
Kansas has 631 Towns.
Kentucky has 467 Towns.
Louisiana has 398 Towns.
Maine has 111 Towns.
Maryland has 368 Towns.
Massachusetts has 351 Towns.
Michigan has 630 Towns.
Minnesota has 867 Towns.
Mississippi has 329 Towns.
Missouri has 972 Towns.
Montana has 275 Towns.
Nebraska has 537 Towns.
Nevada has 71 Towns.
New Hampshire has 234 Towns.
New Jersey has 505 Towns.
New Mexico has 234 Towns.
New York has 1050 Towns.
North Carolina has 655 Towns.
North Dakota has 373 Towns.
Ohio has 1054 Towns.
Oklahoma has 691 Towns.
Oregon has 309 Towns.
Pennsylvania has 1401 Towns.
Rhode Island has 27 Towns.
South Carolina has 368 Towns.
South Dakota has 350 Towns.
Tennessee has 382 Towns.
Texas has 1510 Towns.
Utah has 289 Towns.
Vermont has 67 Towns.
Virginia has 371 Towns.
Washington has 522 Towns.
West Virginia has 282 Towns.
Wisconsin has 630 Towns.
Wyoming has 198 Towns.
Puerto Rico has 225 Towns.
