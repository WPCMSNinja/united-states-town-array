<?php
/*
US Towns Array
Greg Whitehead
http://wpcms.ninja

This file looks at the us town array and displays the total town count overall and for each state.  It then puts out each town with state abbreviation.
*/
include('us_town_array.php');

$townlist = '';
$statelist = "<table>";
$maintotal = 0;
foreach ($states as $name => $info) {
	$totalCount = count($info['TOWNS']);
	$maintotal += $totalCount;
	$statelist .= "<tr><td>" .$name . " has </td><td>" . $totalCount . " Towns.</td></tr>\n";
	foreach ($info['TOWNS'] as $town) {
		$townlist .= $town.", ".$name."<br>\n";
		foreach ($info['ABBRV'] as $abbrv) {
			$townlist .= $town.", ". $abbrv . "<br>\n";
		}
	}
}
echo "Total Count: ". $maintotal . "<br>";
echo $statelist;
echo "</table>";
echo $townlist;


?>